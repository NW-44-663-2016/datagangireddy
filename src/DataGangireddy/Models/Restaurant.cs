﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataGangireddy.Models
{
    public class Restaurant
    {
        [ScaffoldColumn(false)]
        [Key]
        public int RestaurantID { get; set; }

        [Display(Name = "Restaurant Name")]
        public string RestaurantName { get; set; }
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Website")]

        public string Website { get; set; }

        

        [ScaffoldColumn(false)]
        public int LocationID { get; set; }


        // Navigation Property
        public virtual Location Location { get; set; }

    }
}
